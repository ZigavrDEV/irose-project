package com.example.irose_project;

import java.util.ArrayList;

import com.example.irose_project.Models.Tecajnica;
import com.example.irose_project.Models.Tecaj;

public final class SingletonBaza {

    private static SingletonBaza INSTANCE;

    private ArrayList<Tecajnica> Vse_Tecajnice = new ArrayList<Tecajnica>();
    private ArrayList<String> Vse_Oznake = new ArrayList<String>();

    public static SingletonBaza getInstance(){
        if(INSTANCE == null)
            INSTANCE = new SingletonBaza();

        return INSTANCE;
    }

    private SingletonBaza(){

    }

    public ArrayList<Tecajnica> getVse_Tecajnice() {
        return Vse_Tecajnice;
    }

    public void setVse_Tecajnice(ArrayList<Tecajnica> vse_Tecajnice) {
        //Ob inicializaciji vseh tecajnic se nastavim vse oznake, ki jih nato uporabljam pri izpisu tabele
        for(Tecaj tecaj : vse_Tecajnice.get(0).getTecaji()){
            Vse_Oznake.add(tecaj.getOznaka());
        }
        Vse_Oznake.sort(String::compareToIgnoreCase);

        this.Vse_Tecajnice = vse_Tecajnice;
    }

    public ArrayList<String> getVse_Oznake() {
        return Vse_Oznake;
    }
}
