package com.example.irose_project.Models;

import java.util.ArrayList;

public class Tecajnica {
    private String datum;
    private ArrayList<Tecaj> tecaji;

    public Tecajnica(){
        tecaji = new ArrayList<Tecaj>();
    }

    public String getDatum() {
        return datum;
    }
    public void setDatum(String datum) {
        this.datum = datum;
    }
    public ArrayList<Tecaj> getTecaji() {
        return tecaji;
    }
    public void setTecaji(ArrayList<Tecaj> tecaji) {
        this.tecaji = tecaji;
    }
    public void addTecaj(Tecaj tecaj){
        tecaji.add(tecaj);
    }

    //Uporabljam pri izpisu tecajnic v tabelo, kjer ima vsak stolpec svojo oznako, in s to oznako nato poiščem pravilno vrednost ki spada v tisti stolpec
    public Tecaj findTecajZOznako(String oznaka){
        return tecaji.stream().filter(tecaj -> oznaka.equals(tecaj.getOznaka())).findFirst().orElse(new Tecaj());
    }

    @Override
    public String toString() {
        return "Tecajnica [datum=" + datum + ", tecaji=" + tecaji + "]";
    }

}
