package com.example.irose_project.Models;

public class Tecaj {
    
    private String oznaka;
    private String sifra;
    private String vrednost;
    
    public Tecaj(){
        
    }

    public String getVrednost() {
        return vrednost;
    }

    public void setVrednost(String vrednost) {
        this.vrednost = vrednost;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    @Override
    public String toString() {
        return "Tecaj [oznaka=" + oznaka + ", sifra=" + sifra + ", vrednost=" + vrednost + "]";
    }
}
