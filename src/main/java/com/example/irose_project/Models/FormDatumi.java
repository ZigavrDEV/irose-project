package com.example.irose_project.Models;

import java.util.LinkedHashMap;
import java.util.Map;

public class FormDatumi {

    private String Zacetni_Datum;
    private String Koncni_Datum;

    private Map<String, Boolean> Oznake = new LinkedHashMap<String, Boolean>();

    public String getZacetni_Datum() {
        return Zacetni_Datum;
    }

    public Map<String, Boolean> getOznake() {
        return Oznake;
    }

    public void setOznake(Map<String, Boolean> oznake) {
        this.Oznake = oznake;
    }

    public void dodajOznako(String oznaka){
        if(oznaka.equals("USD") || oznaka.equals("GBP"))
            Oznake.put(oznaka, true);
        else
            Oznake.put(oznaka, false);
    }

    public void setZacetni_Datum(String zacetni_Datum) {
        Zacetni_Datum = zacetni_Datum;
    }

    public String getKoncni_Datum() {
        return Koncni_Datum;
    }

    public void setKoncni_Datum(String koncni_Datum) {
        Koncni_Datum = koncni_Datum;
    }

    @Override
    public String toString() {
        return "FormDatumi [Koncni_Datum=" + Koncni_Datum + ", Oznake=" + Oznake + ", Zacetni_Datum=" + Zacetni_Datum
                + "]";
    }
    
}
