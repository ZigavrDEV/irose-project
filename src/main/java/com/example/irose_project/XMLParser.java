package com.example.irose_project;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.example.irose_project.Models.Tecaj;
import com.example.irose_project.Models.Tecajnica;


public class XMLParser {
    static final String DTECBS = "DtecBS";
    static final String TECAJNICA = "tecajnica";
    static final String DATUM = "datum";
    static final String TECAJ = "tecaj";
    static final String OZNAKA = "oznaka";
    static final String SIFRA = "sifra";

    public ArrayList<Tecajnica> preberiXML() throws MalformedURLException, IOException {
        
        //Incializacija in uporaba eventov kopiranih iz https://www.vogella.com/tutorials/JavaXML/article.html
        ArrayList<Tecajnica> vseTecajnice = new ArrayList<Tecajnica>();
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();

            //Prebere datoteko
            InputStream in = new URL("https://www.bsi.si/_data/tecajnice/dtecbs-l.xml").openStream();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            //Inicializacija objektov
            Tecajnica tecajnica = null;
            Tecaj tecaj = null;
            Iterator<Attribute> lastnosti = null;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    String elementName = startElement.getName().getLocalPart();
                    
                    switch (elementName) {
                        case TECAJNICA:
                            tecajnica = new Tecajnica();
                            //Preko iteratorja preberemo vse atribute in jih nato dodamo tecajnici
                            lastnosti = startElement.getAttributes();
                            while (lastnosti.hasNext()) {
                                Attribute lastnost = lastnosti.next();
                                if (lastnost.getName().toString().equals(DATUM)) {
                                    tecajnica.setDatum(lastnost.getValue());
                                }
                            }
                            break;
                        case TECAJ:
                            tecaj = new Tecaj();
                            tecaj.setVrednost(eventReader.nextEvent().asCharacters().getData());

                            lastnosti = startElement.getAttributes();
                            while (lastnosti.hasNext()) {
                                Attribute lastnost = lastnosti.next();
                                if (lastnost.getName().toString().equals(OZNAKA)) {
                                    tecaj.setOznaka(lastnost.getValue());
                                }
                                if (lastnost.getName().toString().equals(SIFRA)) {
                                    tecaj.setSifra(lastnost.getValue());
                                }
                            }
                            tecajnica.addTecaj(tecaj);
                            break;
                    }
                }
                
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals(TECAJNICA)) {
                        vseTecajnice.add(tecajnica);
                    }
                }

            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return vseTecajnice;
    }
}
