package com.example.irose_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;


@RestController
@SpringBootApplication
public class IroseProjectApplication {

	public static void main(String[] args) throws Exception {
		SingletonBaza baza = SingletonBaza.getInstance();

		System.out.println("Nalaganje podatkov...");
		XMLParser parser = new XMLParser();
		baza.setVse_Tecajnice(parser.preberiXML());
		System.out.println("Nalaganje koncano");

		SpringApplication.run(IroseProjectApplication.class, args);
	}
}
