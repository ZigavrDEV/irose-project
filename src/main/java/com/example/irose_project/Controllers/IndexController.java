package com.example.irose_project.Controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.example.irose_project.SingletonBaza;
import com.example.irose_project.Models.FormDatumi;
import com.example.irose_project.Models.Tecaj;
import com.example.irose_project.Models.Tecajnica;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {

	SingletonBaza baza = SingletonBaza.getInstance();

	// Uporabljam za izpis tabele
	ArrayList<Tecajnica> izbrane_tecajnice = new ArrayList<Tecajnica>();
	ArrayList<String> izbrane_oznake = new ArrayList<String>();

	//Uporabljam pri izrisu grafov - x os
	ArrayList<String> izbrani_datumi = new ArrayList<String>();
	// Za pregled datumov
	SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

	@GetMapping("/index")
	public String index(FormDatumi formDatumi, Model model) {

		
		// V formDatumih imam LinkedHashedMap v katero vstavim value in key.
		// Value = Oznaka, Key = True/False, to povežem z checkboxom in v post metodi
		// dobim list katere vrednosti so "checked"
		for (String oznaka : baza.getVse_Oznake()) {
			formDatumi.dodajOznako(oznaka);
		}

		// Nastavim privzeto vrednost datumov
		formDatumi.setZacetni_Datum("2021-10-24");
		formDatumi.setKoncni_Datum("2021-10-27");

		// Pošljem null zato, da se ne izriše prazna tabela
		model.addAttribute("list", null);
		return "index";
	}

	@PostMapping("/index")
	public void posodobiListo(FormDatumi formDatumi, Model model) throws ParseException {

		// Resetiranje filtrov
		izbrane_tecajnice.clear();
		izbrane_oznake.clear();
		izbrani_datumi.clear();

		Date izbran_zacetni_Datum = formater.parse(formDatumi.getZacetni_Datum());
		Date izbran_koncni_Datum = formater.parse(formDatumi.getKoncni_Datum());

		//Map<Oznaka, Vrednosti>
		Map<String, ArrayList<String>> vse_tocke = new LinkedHashMap<String, ArrayList<String>>();

		for (Map.Entry<String, Boolean> izbrana_oznaka : formDatumi.getOznake().entrySet()) {
			if (izbrana_oznaka.getValue() != null && izbrana_oznaka.getValue()) {
				// Ime oznake npr: USD, AUD...
				izbrane_oznake.add(izbrana_oznaka.getKey());
				vse_tocke.put(izbrana_oznaka.getKey(), new ArrayList<String>());
			}
			// Oznake ki so "false" imajo vrednost null, in jih zato nastavim na false, ker checkbox ne prejema null vrednosti
			else {
				izbrana_oznaka.setValue(false);
			}
		}

		for (Tecajnica tecajnica : baza.getVse_Tecajnice()) {
			Date datum_tecajnice = (formater.parse(tecajnica.getDatum()));

			//Ce se datum nahaja med in z izbranimi datumi
			if ((datum_tecajnice.equals(izbran_zacetni_Datum) || datum_tecajnice.equals(izbran_koncni_Datum)) || 
				(datum_tecajnice.after(izbran_zacetni_Datum) && datum_tecajnice.before(izbran_koncni_Datum)))
			{
				//Za izpis tabele, v front endu formatiram izpis
				izbrane_tecajnice.add(tecajnica);

				//Uporabljam pri izrisu grafa: x-os vrednosti
				izbrani_datumi.add(tecajnica.getDatum());

				//Grem cez vse tecaje in pri izbranih oznakah dodam v list vrednost tecaja pri izbrani oznaki
				for (Tecaj tecaj : tecajnica.getTecaji()) {
					if(izbrane_oznake.contains(tecaj.getOznaka()))
						vse_tocke.get(tecaj.getOznaka()).add(tecaj.getVrednost());
				}
			}
			// Ce je datum tecajnice vecji od koncnega datuma se loop zapre
			if (datum_tecajnice.after(izbran_koncni_Datum)) {
				break;
			}
		}
		
		//Pošiljam podatke v front end
		model.addAttribute("datumi", izbrani_datumi);
		model.addAttribute("vseTocke", vse_tocke);
		model.addAttribute("list", izbrane_tecajnice);
		model.addAttribute("oznake", izbrane_oznake);
	}


	/*
	// Manualna posodobitev tecajnic
	@PostMapping("/index/posodobi")
	public String posodobiListTecajnic() throws MalformedURLException, IOException {
		XMLParser parser = new XMLParser();
		baza.setVse_Tecajnice(parser.preberiXML());
		return "List posodobljen";
	}
	*/
}