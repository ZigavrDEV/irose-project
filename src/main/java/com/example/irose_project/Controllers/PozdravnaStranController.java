package com.example.irose_project.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PozdravnaStranController {
    
    @GetMapping("/")
	public String pozdravnaStran() {
		return "pozdravnaStran";
	}
}
